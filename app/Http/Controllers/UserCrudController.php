<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UserCrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = DB::table('users_crud')->get();
        return view('list')->with('user_crud',$data);
        // dd($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'fname' => 'required|min:3|max:10',
            'lname' => 'required|min:3|max:10',
            'age' => 'required|numeric'
        ]);

       DB::table('users_crud')->insert([
            'fname' => $request->fname,
            'lname' => $request->lname,
            'age' => $request->age,
            'created_at' => now()
            
       ]);
       $request->Session()->flash('message', "Create Succes");
       return redirect('list');
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('users_crud')->find($id);
        return view('edit')->with('userid',$data);
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'fname' => 'required|min:3|max:10',
            'lname' => 'required|min:3|max:10',
            'age' => 'required|numeric'
        ]);
        DB::table('users_crud')
            ->where('id', '=', $id)
            ->update([
                'fname' => $request->fname,
                'lname' => $request->lname,
                'age' => $request->age,
                'updated_at' => now()
            ]);
            $request->Session()->flash('message', "Update Succes");
            return redirect('list');
           
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('users_crud')->where('id','=',$id)->delete();
        $request->Session()->flash('message', "Delete Succes");
        return redirect('list');
        
    }
}
