<?php

use Illuminate\Database\Seeder;

class user_crudTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_crud')->insert([
            'fname' => "OG",
            'lname' => "win",
            'age' => 20
        ]);
    }
}
