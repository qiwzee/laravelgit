<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('layout.master');
});

Route::get('/list', function () {
    return view('list');
});
Route::get('/add', function () {
    return view('add');
});
Route::get('/edit', function () {
    return view('edit');
});
Route::resource('users_crud', 'UserCrudController');
Route::resource('list', 'UserCrudController');
// Route::get('/user', function () {
//     return view('user');
// });
// Route::view('/user', 'user')->name('user.c'); 
// Route::get('user/{id}', function ($id) {
//     return view('user')->with('id',$id);
    
// });

// Route::get('user/{id}', 'UserController@index');

// Route::prefix('user')->group(function(){
//     // Route::get('{id}','UserController@index');
//     Route::get('{id}',function(){
//         return "test";
//     })->where('id','[0-9]+');
// });
