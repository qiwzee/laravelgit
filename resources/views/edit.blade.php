@extends('layout.master')
@section('title','Edit')
@section('content')
    <form action="{{ url('users_crud',[$userid->id]) }}" method="post">
        @csrf
        @method('PUT')
            
       
        <div class="form-group">
            <label>Fristname</label>
        <input type="text" class="form-control" name="fname" value="{{ $userid->fname }}">
            <label>Lastname</label>
            <input type="text" class="form-control" name="lname" value="{{ $userid->lname }}">
        </div>
        <div class="form-group">
                <label>Age</label>
                <input type="text" class="form-control" name="age" value="{{ $userid->age }}">
        </div>
        <button type="submit" class="btn btn-success">Save</button>

        @if ($errors-> any())
        <div class="alert alert-danger mt-5">
            <ul>
                @foreach ($errors->all() as $item)
                <li>{{ $item }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </form>
@endsection