@extends('layout.master')
@section('title','list')
{{-- @section('css')
    
@endsection --}}
@section('content')
    @if (Session::has('message'))
    <div class="alert alert-success alert-dismissible" > 
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ Session::get('message') }}
    </div>
    @endif
    <div class="text-wrap">
        <h1 class="display-4 fontCh">List Page</h1>
        
    </div>
    <div class="form control">
        <a href="add" class="btn btn-info mt-3 col-12">ADD</a>
    </div>
    <table class="table table-dark mt-3">
            <thead>
                <th>ID</th>
                <th>Fname</th>
                <th>Lname</th>
                <th>age</th>
                <th>Creat Date</th>
                <th>Update Date</th>
                <th>action</th>
               
            <tbody>
                @foreach ($user_crud as $p)
                <tr>
                    <td>{{ $p->id }}</td>
                    <td>{{ $p->fname }}</td>
                    <td>{{ $p->lname }}</td>
                    <td>{{ $p->age }}</td>
                    <td>{{ date('d-m-y H:i:s',strtotime($p->created_at)) }}</td>
                    <td>{{ date('d-m-y H:i:s',strtotime($p->updated_at)) }}</td>
                <td class="form-inline justify-content-center"><a href="users_crud/{{ $p->id }}/edit" class="btn btn-success">Update</a>
               
                    <form class="ml-2" action="{{ url('users_crud',[$p->id]) }}" method="POST">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
                </tr>
                    
                @endforeach
                <tr>
                    <td></td>
                </tr>
            </tbody>
        </table>
@endsection