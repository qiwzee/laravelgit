@extends('layout.master')
@section('title','Add')
@section('content')
    <form action="{{ url('users_crud') }}" method="post">
        @csrf
        <div class="form-group">
            <label>Fristname</label>
            <input type="text" class="form-control" name="fname">
            <label>Lastname</label>
            <input type="text" class="form-control" name="lname">
        </div>
        <div class="form-group">
                <label>Age</label>
                <input type="text" class="form-control" name="age">
        </div>
        <button type="submit" class="btn btn-success">Save</button>

        @if ($errors-> any())
        <div class="alert alert-danger mt-5">
            <ul>
                @foreach ($errors->all() as $item)
                <li>{{ $item }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </form>
@endsection